#  Makefile for SIMLIB
#  ===================

# name of the compiler for C++ language
CXX?=g++

# C++ compiler flags
CXXFLAGS  = -Wall
#CXXFLAGS += -std=c++98
CXXFLAGS += -m64
#CXXFLAGS += -O2 # add optimization level
CXXFLAGS += -g  # add debug info
#CXXFLAGS += -pg # add profiling support
#CXXFLAGS += -Weffc++ # add extra checking of source code

SIMLIB_DIR=.
SIMLIB_DEPEND=simlib.so simlib.h
CXXFLAGS += -I$(SIMLIB_DIR)

% : %.cc
	@#$(CXX) $(CXXFLAGS) -static -o $@  $< $(SIMLIB_DIR)/simlib.a -lm
	$(CXX) $(CXXFLAGS) -o $@  $< $(SIMLIB_DIR)/simlib.so -lm

main: main.cc $(SIMLIB_DEPEND)
