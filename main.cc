#include "simlib.h"
#include "TextTable.h"

#include <iostream>
#include <fstream>

using namespace std;

TStat produkce("Produkce");
Facility generace;
double spotreba[12];
double maxSpicka = 15e6;
//Soucasny mesic pouzity v simulaci
int mesic = 0;
//Vyroba v Pardubickem kraji
const double rocni_spotreba = 81.9e12;
//Histogram spotreba;
double mesicni_podily[12] = {
    9.38 * 1e-2,
    8.96 * 1e-2,
    9.54 * 1e-2,
    7.71 * 1e-2,
    7.80 * 1e-2,
    7.54 * 1e-2,
    7.43 * 1e-2,
    7.70 * 1e-2,
    7.50 * 1e-2,
    8.46 * 1e-2,
    9.00 * 1e-2,
    9.02 * 1e-2
};
double mesicni_spotreba[12];

int hodiny_v_mesici[12] = {
    31 * 24,
    28 * 24,
    31 * 24,
    30 * 24,
    31 * 24,
    30 * 24,
    31 * 24,
    31 * 24,
    30 * 24,
    31 * 24,
    30 * 24,
    31 * 24
};

class Elektrarna {

    private:
        double vyuziti;

    protected:
        double podil;
        tuple<double, double> USPerKWh;
        tuple<double, double> KcPerKWh;
        string typ;
        double MWPerHa;

        double sumy[4] = {0, };
 
        virtual double getVyuziti(){
            return vyuziti;
        }
    public:
        double US[12] = {0, };
        double Wh[12] = {0, };
        double Kc[12] = {0, };
        Elektrarna(double vyuziti,
            tuple<double, double> USPerKWh, 
            tuple<double, double> kcPerKWh, 
            string typ,
            double MWPerHa,
            double podil) : vyuziti(vyuziti), podil(podil), USPerKWh(USPerKWh), KcPerKWh(kcPerKWh), MWPerHa(MWPerHa), typ(typ) {}
        virtual void Generate() = 0;
        double potrebnaPlochaVHektarech(){
            return (getWatts() * 1e6) / MWPerHa;
        }
        // pro dany typ elektrene
        virtual double getSpotrebaMesicniWattHodiny(){
            return podil * mesicni_spotreba[mesic];
        }
        double getCashNeeded(){
            return Uniform(get<0>(KcPerKWh), get<1>(KcPerKWh)) * (getSpotrebaMesicniWattHodiny() / 1000);
            double stred = (get<0>(KcPerKWh) + get<1>(KcPerKWh)) / 2;
            double roztpyl = abs(get<1>(KcPerKWh) - stred);
            return (Normal(stred, roztpyl)) * (getSpotrebaMesicniWattHodiny() / 1000);
        }
        virtual double getWatts(){
            return getSpotrebaMesicniWattHodiny() / hodiny_v_mesici[mesic] / getVyuziti();
        }
        double getUhlikovaStopa(){
            return Uniform(get<0>(USPerKWh), get<1>(USPerKWh)) * (getSpotrebaMesicniWattHodiny() / 1000);
            double stred = (get<0>(USPerKWh) + get<1>(USPerKWh)) / 2;
            double roztpyl = abs(get<1>(USPerKWh) - stred);
            return Normal(stred, roztpyl) * (getSpotrebaMesicniWattHodiny() / 1000);
        }
        void prictiDoSum(double Wh, double W, double US, double kc){
            sumy[0] += Wh;
            sumy[1] += W;
            sumy[2] += US;
            sumy[3] += kc;
            this->US[mesic] += US;
            this->Kc[mesic] += kc;
            this->Wh[mesic] += Wh;
        }
        void vypisSumy(TextTable& table){
            table.add(this->getTyp());
            table.add(to_string(sumy[0] * 1e-12)); 
            table.add(to_string(sumy[1] / 12 * 1e-9)); 
            table.add(to_string(sumy[2] * 1e-6));  
            table.add(to_string(sumy[3])); 
            table.add(to_string(sumy[3] / sumy[0] * 1e3));
            table.endOfRow();
        }
        string getTyp(){
            return typ;
        }
        virtual bool konstantniVykon(){
            return true;
        }
        virtual string getReference() = 0;

};

class Solar : public Elektrarna {

    protected:
    /*double ucinnost[12] = {
        0.035684,
        0.051827,
        0.083263,
        0.103653,
        0.125743,
        0.117247,
        0.13339,
        0.122345,
        0.091759,
        0.075616,
        0.033135,
        0.026338
    };*/
    //Pocet Wh v danem mesici na 1 Wp panelu
    double ucinnost[12] = {
        42,
        61,
        98,
        122,
        148,
        138,
        157,
        144,
        108,
        89,
        39,
        31
    };
    public:

        Solar(double podil): Elektrarna(0, {35, 50}, {1, 3}, "Solar", 0.300, podil) {};
        void Generate(){
        }
        double getSpotrebaMesicniWattHodiny(){
            return podil * ucinnost[mesic];
        }

        //Wp
        double getWatts(){
            return getSpotrebaMesicniWattHodiny() / ucinnost[mesic];
        }

        string getReference(){
            double ref = (getWatts() * 1e-3) / 3;
            return to_string(ref) + "x 3kW";
        }

        bool konstantniVykon(){
            return false;
        }
};

class Jaderka : public Elektrarna {
    public:
        // 0,84 ucinnost upravena
        Jaderka(double podil) : Elektrarna(0.75, {10, 35}, {3.7, 4.3}, "Jaderna", 18.181818, podil) {}
        void Generate(){
        }

        string getReference(){
            double ref = (getWatts() * 1e-6) / 500;
            return to_string(ref) + "x 500MW";
        }
};

class Plynarna : public Elektrarna {
    public:
        Plynarna(double podil) : Elektrarna(0.9982, {450, 650}, {1.9, 2.5}, "Plynova", 4.405, podil) {}
        void Generate(){
        }

        string getReference(){
            double ref = (getWatts() * 1e-6) / 440;
            return to_string(ref) + "x 440MW";
        }
};

class Vetrone : public Elektrarna {
    /*
    std::tuple<double, double> mesicni_vykyvy[12] = {
        {0.097292724, 0.125333508},
        {0.046118497, 0.135842222},
        {0.085731536, 0.105430516},
        {0.076071644, 0.095355326},
        {0.056683587, 0.088549004},
        {0.041457034, 0.065651438},
        {0.046610865, 0.065710933},
        {0.043492532, 0.051720668},
        {0.050513182, 0.071639785},
        {0.060010497, 0.112916462},
        {0.096277496, 0.123722352},
        {0.113819302, 0.143824027},
    };*/
    tuple<double, double> ucinnosti[12] = {
        {192.8574, 248.4409},
        {91.41785, 269.2716},
        {169.9403, 208.9884},
        {150.7921, 189.017},
        {112.3604, 175.5252},
        {82.17772, 130.1368},
        {92.39384, 130.2547},
        {86.21256, 102.5227},
        {100.1292, 142.0071},
        {118.9551, 223.8273},
        {190.8449, 245.2472},
        {225.617, 285.0935}
    };
    //Pocet Wh v danem mesici na 1 W turbiny
    // std::tuple<double, double> ucinnosti[12] = {
    //     {30.456, 39.269},
    //     {14.435, 42.518},
    //     {26.833, 32.999},
    //     {23.810, 29.842},
    //     {17.741, 27.715},
    //     {12.175, 20.548},
    //     {14.589, 20.567},
    //     {13.613, 16.188},
    //     {15.816, 22.423},
    //     {18.783, 35.342},
    //     {30.134, 38.735},
    //     {35.625, 45.016}
    // };

    public:
        Vetrone(double podil) : Elektrarna(0, {8, 20}, {1, 3}, "Vetrna", 0.0769, podil){}
        double getVyuziti(){
            return Uniform(get<0>(ucinnosti[mesic]), get<1>(ucinnosti[mesic]));
            double stred = (get<0>(ucinnosti[mesic]) + get<1>(ucinnosti[mesic])) / 2;
            double roztpyl = get<1>(ucinnosti[mesic]) - stred;
            
            return Normal(stred, roztpyl);
        }
        double getSpotrebaMesicniWattHodiny(){
            return podil * getVyuziti();
        }

        double getWatts(){
            return getSpotrebaMesicniWattHodiny() / getVyuziti();
        }
        void Generate(){
        }

        string getReference(){
            double ref = (getWatts() * 1e-6) / 2;
            return to_string(ref) + "x 2MW";
        }

        bool konstantniVykon(){
            return false;
        }
};

class Uhelna : public Elektrarna {
    public:
        // 0,75 ucinnost upravena
        Uhelna(double podil): Elektrarna(0.40, {850, 1050}, {1.2, 2.2}, "Uhelna", 6.25, podil) {}
        void Generate(){
        }

        string getReference(){
            double ref = (getWatts() * 1e-6) / 1000;
            return to_string(ref) + "x 1000MW";
        }
};

class Biomasa : public Elektrarna {
    public:
        Biomasa(double podil): Elektrarna(0.35, {10, 50}, {2.5, 3.7}, "Biomasa", 6.6666666, podil){}
        void Generate(){
        }

        string getReference(){
            double ref = (getWatts() * 1e-6) / 5.6;
            return to_string(ref) + "x 5.6MW";
        }
};

class Vodni : public Elektrarna {
    public:
        Vodni(double podil) : Elektrarna(0.12, {5, 15}, {1.7, 2.1}, "Vodni", 21.739, podil){}
        void Generate(){
        }

        string getReference(){
            double ref = (getWatts() * 1e-6) / 144;
            return to_string(ref) + "x 144MW";
        }
};

std::vector<Elektrarna *> elektrarny;

class Generator : public Event {

    double rocniSuma[4] = {0,};

    public: 
        void Behavior(){
            Print("%d. mesic: \n", mesic + 1);
            TextTable table;
            table.add("Typ");
            table.add("Wh [GWh]");
            table.add("W [GW]");
            table.add("US [t]");
            table.add("Penizky [kc]");
            table.add("Penizky [kc/kWh]");
            table.add("Reference");
            table.endOfRow();
            for (size_t i = 0; i < elektrarny.size(); i++){
                Elektrarna *e = elektrarny.at(i);
                if (!e->konstantniVykon()){
                    mesicni_spotreba[mesic] -= e->getSpotrebaMesicniWattHodiny();
                }
            }
            double WhSum = 0;
            double WSum = 0;
            double USSum = 0;
            double KcSum = 0;
            for (size_t i = 0; i < elektrarny.size(); i++)
            {
                Elektrarna *e = elektrarny.at(i);
                double Wh = e->getSpotrebaMesicniWattHodiny();
                WhSum += Wh;
                double W = e->getWatts();
                WSum += W;
                double us = e->getUhlikovaStopa();
                USSum += us;
                double kc = e->getCashNeeded();
                KcSum += kc;
                table.add(e->getTyp());
                table.add(to_string(Wh * 1e-9));
                table.add(to_string(W * 1e-9));
                table.add(to_string(us * 1e-6));
                table.add(to_string(kc));
                table.add(to_string(kc/Wh * 1e3));
                table.add(e->getReference());
                table.endOfRow();
                e->prictiDoSum(Wh, W, us, kc);
            }
            rocniSuma[0] += WhSum;
            rocniSuma[1] += WSum;// irelevantni
            rocniSuma[2] += USSum; 
            rocniSuma[3] += KcSum;
            table.endOfRow();
            table.add("Celkem");
            table.add(to_string(WhSum * 1e-9));
            table.add(to_string(WSum * 1e-9));
            table.add(to_string(USSum  * 1e-6));
            table.add(to_string(KcSum));
            table.add(to_string(KcSum/WhSum * 1e3));
            table.add(" ");
            table.endOfRow();

            table.setAlignment(2, TextTable::Alignment::LEFT);
            std::cout << table;
    
            Activate(Time+1);
            mesic++;
            if (mesic == 12){
                cout << endl;
                cout << endl;

                TextTable sum;
                sum.add(" ");
                sum.add("Wh [TWh]");
                sum.add("US [t]");
                sum.add("Penizky [kc]");
                sum.add("Penizky [kc/kWh]");
                sum.endOfRow();
                sum.add("Celkem");
                sum.add(to_string(rocniSuma[0] * 1e-12)); 
                sum.add(to_string(rocniSuma[2] * 1e-6));  
                sum.add(to_string(rocniSuma[3])); 
                sum.add(to_string(rocniSuma[3] / rocniSuma[0] * 1e3));
                sum.endOfRow();
                sum.setAlignment(2, TextTable::Alignment::LEFT);
                cout << sum;
            }
        }
};

void vygenerujSpotrebuProRok(){
    for (size_t i = 0; i < 12; i++)
    {
        mesicni_spotreba[i] = rocni_spotreba * (mesicni_podily[i]);
    }
}

void vypisSpotrebuNaRok(){
    TextTable table;
    table.add("Mesic");
    table.add("Predpokladana mesicni spotreba");
    table.endOfRow();
    for (size_t i = 0; i < 12; i++)
    {
        table.add(std::to_string(i + 1));
        table.add(std::to_string(mesicni_spotreba[i]) + " Wh");
        table.endOfRow();
    }

    table.setAlignment(2, TextTable::Alignment::RIGHT);
    std::cout << table;
    
}

void zpracujVstupy(int argc, char *argv[]){
    ifstream f;
    f.open(argv[1]);
    if (f.is_open()){
        string line;
        double total = 1.0;
        double zbyle_procenta = 0;
        vector<tuple<string, double>> procenta;
        while(getline(f, line)){
            int delimPos = line.find(":");
            string type = line.substr(0, delimPos);
            double percent = 0.01 * stod(line.substr(delimPos+1));
            total -= percent;
            if (type == "s") {
                percent = (percent  * rocni_spotreba) / 1000;
            } else if (type == "v") {
                percent = (percent * rocni_spotreba) / 1982;
            } else {
                zbyle_procenta += percent;
            }
            procenta.push_back({type, percent});
        }
        

        for (size_t i = 0; i < procenta.size(); i++)
        {
            string type = get<0>(procenta.at(i));
            double percent = get<1>(procenta.at(i));
     
            switch(tolower(type.at(0))){
                case 'w':
                    percent=(percent/zbyle_procenta);
                    elektrarny.push_back(new Vodni(percent));
                    break;
                case 'b':
                    percent=(percent/zbyle_procenta);
                    elektrarny.push_back(new Biomasa(percent));
                    break;
                case 'v':
                    elektrarny.push_back(new Vetrone(percent));
                    break;
                case 'u':
                    percent=(percent/zbyle_procenta);
                    elektrarny.push_back(new Uhelna(percent));
                    break;
                case 'p':
                    percent=(percent/zbyle_procenta);
                    elektrarny.push_back(new Plynarna(percent));
                    break;
                case 'j':
                    percent=(percent/zbyle_procenta);
                    elektrarny.push_back(new Jaderka(percent));
                    break;
                case 's':
                    elektrarny.push_back(new Solar(percent));
                    break;
                default:
                    cerr << "Necos zadal spatne";
                    exit(1);
            }
            std::cout << type << " " << percent << endl;   

        }
        
        std::cout << "Total percentage: " << total << endl;
        f.close();
        // if (total < 0){
        //     cerr << "Moc elektriny chces! ";
        //     exit(1);
        // }
    }
}

void grafUS(){
    ofstream grafData;
    grafData.open("USgrafData");
    grafData << "# Mesicni produkce US dle typu elektrarny" << endl;
    grafData << "Mesic" << "\t";
    for (size_t i = 0; i < elektrarny.size(); i++){
        grafData << elektrarny.at(i)->getTyp() << "\t";
    }
    grafData << endl;
    for (size_t i = 0; i < 12; i++){
        grafData << to_string(i + 1) << "\t";
        for (size_t j = 0; j < elektrarny.size(); j++){
            grafData << elektrarny.at(j)->US[i] << "\t";
        }
        grafData << endl;
        
    }
    grafData << endl;
    grafData.close();
}

void grafWh(){
    ofstream grafData;
    grafData.open("WhgrafData");
    grafData << "# Mesicni produkce Wh dle typu elektrarny" << endl;
    grafData << "Mesic" << "\t";
    for (size_t i = 0; i < elektrarny.size(); i++){
        grafData << elektrarny.at(i)->getTyp() << "\t";
    }
    grafData << endl;
    for (size_t i = 0; i < 12; i++){
        grafData << to_string(i + 1) << "\t";
        for (size_t j = 0; j < elektrarny.size(); j++){
            grafData << elektrarny.at(j)->Wh[i] << "\t";
        }
        grafData << endl;
        
    }
    grafData << endl;
    grafData.close();
}


void grafKc(){
    ofstream grafData;
    grafData.open("KcgrafData");
    grafData << "# Mesicni naklady na produkci Wh dle typu elektrarny" << endl;
    grafData << "Mesic" << "\t";
    for (size_t i = 0; i < elektrarny.size(); i++){
        grafData << elektrarny.at(i)->getTyp() << "\t";
    }
    grafData << endl;
    for (size_t i = 0; i < 12; i++){
        grafData << to_string(i + 1) << "\t";
        for (size_t j = 0; j < elektrarny.size(); j++){
            grafData << elektrarny.at(j)->Kc[i] << "\t";
        }
        grafData << endl;
        
    }
    grafData << endl;
    grafData.close();
}

int main(int argc, char *argv[]){
    //SetOutput("statistiky");
    if (argc < 2){
        cerr << "Dej mi data!" << endl;
        return 1;
    }
    vygenerujSpotrebuProRok();
    zpracujVstupy(argc, argv);
    RandomSeed(std::time(nullptr));
    Init(1, 12);
    (new Generator)->Activate();
    Run();
    grafUS();
    grafWh();
    grafKc();
    TextTable tab;
    tab.add("Nazev");
    tab.add("Wh [TWh]");
    tab.add("Prum GW");
    tab.add("US [t]");
    tab.add("Penizky [kc]");
    tab.add("Penizky [kc/kWh]");
    tab.endOfRow();
    for (size_t i = 0; i < elektrarny.size(); i++)
    {
        elektrarny.at(i)->vypisSumy(tab);
    }
    tab.setAlignment(2, TextTable::Alignment::LEFT);
    cout << tab;

    TextTable csv('\0', '\t', '\0');
    csv.add("Nazev");
    csv.add("Wh [TWh]");
    csv.add("Prum GW");
    csv.add("US [t]");
    csv.add("Penizky [kc]");
    csv.add("Penizky [kc/kWh]");
    csv.endOfRow();
    for (size_t i = 0; i < elektrarny.size(); i++)
    {
        elektrarny.at(i)->vypisSumy(csv);
    }
    ofstream of;
    of.open("csv");
    of << csv;
    of.close();
    //vypisSpotrebuNaRok();
    //produkce.Output();
}