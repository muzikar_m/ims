#!/usr/bin/fish
set exp_name $argv[1]
if not test -d $exp_name 
    mkdir $exp_name
end
./main data > "$exp_name/out"
mv *grafData $exp_name
mv csv $exp_name
cp data $exp_name/data